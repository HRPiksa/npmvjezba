const
    // development mode
    devMode = true,

    // modules
    gulp = require('gulp'),
    newer = require('gulp-newer'),
    htmlclean = require('gulp-htmlclean'),
    noop = require('gulp-noop'),
    sync = require('browser-sync').create(),

    // folders
    src = 'src/',
    build = 'public/';

// HTML processing
function html() {
    return gulp.src(src + 'html/**/*')
        .pipe(newer(build))
        .pipe(devMode ? noop() : htmlclean())
        .pipe(gulp.dest(build))
        .pipe(sync.stream());
}

// watch for file changes
function watch(done) {
    sync.init({
        server: {
            baseDir: './' + build,
            startPath: 'index.html'
        }
    });

    // html changes
    gulp.watch(src + 'html/**/*', html);

    done();
}

// Single tasks
exports.html = html;
exports.watch = watch;

// run all build tasks
exports.build = gulp.parallel(exports.html);

// default task
exports.default = gulp.series(exports.build, exports.watch);
